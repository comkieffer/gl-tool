# Gl-Tool ToDo

## Milestone #1: v0.2.0

### Done

- [X] Core: Make it possible to go directly to code, issues, pipelines, ...
- [X] Conf: Add support for group abbreviations in config file
- [ ] Core: Make it possible to jump to the page url for the project
  WONTFIX: There doesn't seem to be a way of getting the pages url for a project in gitlab at this time.
- [X] Core: Figure out how deprecated projects are handled
  `goto` command now has `--include-archived` flag
- [X] Core: Make it possible to go straight to the current active iteration
  Added 'Current Iteration' in `goto` to groups which have iterations
- [ ] Auth: Use dbus secrets api for https logins
  WONTFIX: using the same interface as the `libsecret` credential helper to connect seems elegant but this is only
  possible in the context of a git repository. There is no generic way of getting the login credentials for a website
  from the system keyring.
- [ ] Auth: Add proper support for ssh + https logins
  WONTFIX: this never made any sense. We're logging in to the gitlab API, to the repos. We need some kind of access
  token to do anything. Instead the recommendation should be to use a read-only API token instead.
- [X] Core: Add 'Activity' view to projects and groups
- [X] Core: Show issues as 'List' or 'Board'
- [X] Core: Show 'Issues' for groups
- [ ] Core: better error handling - catch errors at the global state and show them in a dedicated rofi window.
  WONTFIX: Actually behaving smartly for non-zero exit codes might be complicated since rofi can return many status
  codes. For now we'll log them and see what happens in the future.
- [X] Core: Show 'Milestones' for groups and projects if any are configured
- [X] Core: Show 'Epics' for groups if any are configured
- [X] Core: Add links to "Your Issues", "Your Assigned Merge Requests", ...
- [X] Core: Add support for multiple base groups
- [X] Core: Add support for working without a base group (should just list projects owned by current user)
- [X] Docs: It would be nice to have a minimum amount of documentation for the project.
    - [X] Add README contents with basic usage info
    - [X] Add screen recording of project to explain the purpose
- [X] Core: speedup cache refresh by parallelizing the operation
    - Use `concurrent.futures.ThreadPoolExecutor` and `executor.submit`

## Milestone #X: v0.3.0

- [ ] Core: Migrate to rofi script mode so that we can non-selectable entries
- [ ] Core: Group entries into Groups, Projects, User

## Milestone #X: v0.4.0

- [ ] Core: Add issues to the cache - make it so that we can jump straight to an existing issue.
- [ ] Core: Use full text search on the issues to search for issues.

## Milestone #X: v1.0.0

- [ ] Core: Use `_access_level` attributes instead of `_enabled` attributes.
  This means handling the tri-state logic cleanly with either an extra property or a custom type.
