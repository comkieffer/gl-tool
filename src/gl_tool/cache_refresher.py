import logging
import re
import sys
from collections.abc import Callable
from concurrent.futures import ThreadPoolExecutor, as_completed
from dataclasses import dataclass
from queue import Empty, Queue
from typing import Any

import gitlab
from gitlab.base import RESTObject
from gitlab.exceptions import GitlabListError
from gitlab.v4.objects import Group as GlGroup
from gitlab.v4.objects import Project as GlProject

from rich.console import Console
from rich.traceback import Traceback

from .errors import ReturnCode
from .models import (
    CurrentUserModel,
    EpicModel,
    GroupIteration,
    GroupModel,
    GroupReference,
    MergeRequestModel,
    MilestoneModel,
    ProjectModel,
)


@dataclass
class CacheRefreshOpts:
    exclude_rules: list[str]

    supports_epics: bool
    supports_iterations: bool


@dataclass
class WorkItem:
    fn: Callable[..., None]
    args: tuple[Any, ...]
    description: str | None


class CacheRefresher:
    def __init__(self) -> None:
        self.user: CurrentUserModel | None = None
        self.groups: dict[int, GroupModel] = {}
        self.projects: dict[int, ProjectModel] = {}

        self.job_queue: Queue[WorkItem] = Queue()

    def refresh(
        self, gl: gitlab.Gitlab, max_parallel_jobs: int, opts: CacheRefreshOpts
    ) -> bool:
        """
        Regenerate the cache.

        Args:
            gl (gitlab.Gitlab): The Gitlab API object to use for the calls. The
                instance must already be authenticated with the API.
            max_parallel_jobs (int): The upper limit to the number of jobs the thread
                pool executor can run at once.
            opts: Additional options, see ``CacheRefreshOpts``.


        Returns:
            bool: True if the cache was successfully updated. False otherwise.
        """
        logging.info("Adding initial jobs to job queue ...")
        self.job_queue.put(WorkItem(self.refresh_user, (gl,), "Refresh user"))

        # Only consider groups to which we actually belong.
        # Otherwise, we'll list every single public group on the gitlab instance.
        for grp in gl.groups.list(membership=True, iterator=True):
            if self._is_excluded(grp.full_name, opts.exclude_rules):
                logging.debug(
                    f"Excluding {grp.full_name} (id: {grp.id}) from cache since it "
                    "matches an exclude rule."
                )
                continue

            self.job_queue.put(
                WorkItem(self.refresh_group, (gl, grp, opts), f"Refresh group {grp.id}")
            )

        logging.debug(
            f"Creating thread pool executor with up to {max_parallel_jobs} workers"
        )
        with ThreadPoolExecutor(max_workers=max_parallel_jobs) as executor:
            futures = []

            logging.info("Adding initial jobs to executor ...")
            while True:
                try:
                    new_job = self.job_queue.get(block=True, timeout=5.0)
                except Empty:
                    break

                logging.debug(f"Adding new job to executor: {new_job.description} ...")
                futures.append(executor.submit(new_job.fn, *new_job.args))

            logging.debug("All jobs added, waiting for futures to complete ...")

            completed_jobs = 0
            for future in as_completed(futures):
                completed_jobs += 1

                exc = future.exception()
                if exc is not None:
                    trace = Traceback.from_exception(type(exc), exc, exc.__traceback__)
                    console = Console()
                    console.print(trace)
                    sys.exit(ReturnCode.Failed)

                logging.info(f"Work item {completed_jobs} of {len(futures)} finished")

        return True

    def refresh_user(self, gl: gitlab.Gitlab) -> None:
        assert gl.user is not None
        logging.debug("Refreshing current user information ...")
        self.user = CurrentUserModel(**gl.user.asdict())

    def refresh_group(
        self,
        gl: gitlab.Gitlab,
        group: RESTObject,
        opts: CacheRefreshOpts,
    ) -> None:
        log_prefix = f"Group {group.id:5} ({group.full_name}):"
        if group.id in self.groups:
            logging.debug(f"{log_prefix}: already in cache, skipping.")
            return

        logging.debug(f"{log_prefix}: Fetching group details ...")
        group = gl.groups.get(group.id)
        self.groups[group.id] = GroupModel(**group.asdict())

        self._fetch_subprojects(gl, group, opts)
        self._fetch_shared_projects(gl, group, opts)

        self._fetch_subgroups(group, opts)

        if opts.supports_iterations:
            self._fetch_group_iterations(group)

        self._fetch_group_milestones(group)

        if opts.supports_epics:
            self._fetch_group_epics(group)

        logging.debug(f"{log_prefix}: Group refresh complete.")

    def _fetch_subprojects(
        self,
        gl: gitlab.Gitlab,
        group: GlGroup,
        opts: CacheRefreshOpts,
    ) -> None:
        log_prefix = f"Group {group.id:5} ({group.full_name}):"
        logging.debug(f"{log_prefix}: Fetching subprojects ...")

        # , include_subgroups=True
        projects = group.projects.list(iterator=True, archived=False)

        for proj in projects:
            if self._is_excluded(proj.name_with_namespace, opts.exclude_rules):
                logging.debug(
                    f"{log_prefix} Excluding {proj.name_with_namespace} (id: {proj.id})"
                    " from cache since it matches an exclude rule."
                )
                continue

            if proj.id in self.projects:
                logging.debug(
                    f"{log_prefix} Group sub-project '{proj.name}' (id: {proj.id}) "
                    f"already in cache. Skipping."
                )
                continue

            logging.debug(
                f"{log_prefix}: Found group sub-project {proj.name} (id: {proj.id})"
            )
            self.job_queue.put(
                WorkItem(
                    self.refresh_project,
                    (gl, proj),
                    f"Refresh project {proj.id}",
                )
            )

    def _fetch_shared_projects(
        self,
        gl: gitlab.Gitlab,
        group: GlGroup,
        opts: CacheRefreshOpts,
    ) -> None:
        log_prefix = f"Group {group.id:5} ({group.full_name}):"
        logging.debug(f"{log_prefix}: Fetching shared projects ...")

        # , include_subgroups=True
        shared_projects = group.shared_projects.list(iterator=True, archived=False)
        for proj in shared_projects:
            if self._is_excluded(proj.name_with_namespace, opts.exclude_rules):
                logging.debug(
                    f"{log_prefix} Excluding {proj.name_with_namespace} (id: {proj.id})"
                    " from cache since it matches an exclude rule."
                )
                continue

            if proj.id in self.projects:
                logging.debug(
                    f"{log_prefix} Group shared project '{proj.name}' (id: {proj.id}) "
                    f"already in cache. Skipping."
                )
                continue

            logging.debug(
                f"{log_prefix}: Found shared project {proj.name} (id: {proj.id})"
            )
            self.job_queue.put(
                WorkItem(
                    self.refresh_project,
                    (gl, proj),
                    f"Refresh project {proj.id}",
                )
            )

    def _fetch_subgroups(self, group: GlGroup, opts: CacheRefreshOpts) -> None:
        log_prefix = f"Group {group.id:5} ({group.full_name}):"
        logging.debug(f"{log_prefix}: Fetching subgroups ...")

        group_model = self.groups[group.id]
        subgroups = group.subgroups.list(all=True)
        for subgroup in subgroups:
            if self._is_excluded(subgroup.full_name, opts.exclude_rules):
                logging.debug(
                    f"{log_prefix} Excluding {subgroup.full_name} (id: {subgroup.id}) "
                    "from cache since it matches an exclude rule."
                )
                continue

            logging.debug(
                f"{log_prefix} Found group subgroup {subgroup.name} (id: {subgroup.id})"
            )
            group_model.subgroups.append(GroupReference(**subgroup.asdict()))

    def _fetch_group_iterations(self, group: GlGroup) -> None:
        log_prefix = f"Group {group.id:5} ({group.full_name}):"
        logging.debug(f"{log_prefix}: Fetching iterations ...")

        try:
            for iteration in group.iterations.list(state="opened"):
                # Only process the iteration if it applies to this group otherwise we
                # will end up with many duplicate iterations across groups
                if iteration.group_id != group.id:
                    break

                group_model = self.groups[group.id]
                if group_model.iterations is None:
                    group_model.iterations = []

                assert isinstance(group_model.iterations, list)
                group_model.iterations.append(GroupIteration(**iteration.asdict()))
        except GitlabListError as exc:
            logging.error(f"{log_prefix}: Unable to list iterations. Error was: {exc}.")

    def _fetch_group_milestones(self, group: GlGroup) -> None:
        log_prefix = f"Group {group.id:5} ({group.full_name}):"
        logging.debug(f"{log_prefix}: Fetching milestones ...")

        group_milestones = group.milestones.list(get_all=True)
        if len(group_milestones) > 0:
            group_model = self.groups[group.id]
            group_model.milestones = {
                ms.id: MilestoneModel(**ms.asdict()) for ms in group_milestones
            }

    def _fetch_group_epics(self, group: GlGroup) -> None:
        log_prefix = f"Group {group.id:5} ({group.full_name}):"
        logging.debug(f"{log_prefix}: Fetching epics ...")

        try:
            group_epics = group.epics.list(get_all=True)
            if len(group_epics) > 0:
                group_model = self.groups[group.id]
                group_model.epics = {
                    epic.id: EpicModel(**epic.asdict()) for epic in group_epics
                }

        except GitlabListError as exc:
            logging.error(f"{log_prefix}: Unable to list epics. Error was: {exc}.")

    def refresh_project(self, gl: gitlab.Gitlab, project: RESTObject) -> None:
        log_prefix = f"Project {project.id} ({project.name_with_namespace})"

        if project.id in self.projects:
            logging.warning(f"{log_prefix} Duplicate project: {project.id}")
            return

        logging.debug(f"{log_prefix}: Fetching project details ...")
        project = gl.projects.get(project.id)

        self.projects[project.id] = ProjectModel(**project.asdict())
        self._fetch_project_milestones(project)
        self._fetch_project_open_merge_requests(project)

        logging.debug(f"{log_prefix}: Project refresh complete")

    def _fetch_project_milestones(self, project: GlProject) -> None:
        log_prefix = f"Project {project.id} ({project.name_with_namespace})"
        logging.debug(f"{log_prefix}: Fetching milestones for project ...")
        project_milestones = project.milestones.list(get_all=True)
        if len(project_milestones) > 0:
            project_model = self.projects[project.id]
            project_model.milestones = {
                ms.id: MilestoneModel(**ms.asdict()) for ms in project_milestones
            }

    def _fetch_project_open_merge_requests(self, project: GlProject) -> None:
        log_prefix = f"Project {project.id} ({project.name_with_namespace})"

        project_model = self.projects[project.id]
        if project_model.merge_requests_access_level == "disabled":
            logging.debug(
                f"{log_prefix} Skipping merge requests for {project.name} "
                f"({project.id}). Merge requests access level is "
                f"{project_model.merge_requests_access_level}."
            )
            return

        logging.debug(f"{log_prefix}: Fetching merge requests for project ...")

        project_merge_requests = project.mergerequests.list(
            get_all=True, state="opened"
        )
        if len(project_merge_requests) > 0:
            project_model.merge_requests = {
                mr.id: MergeRequestModel(**mr.asdict()) for mr in project_merge_requests
            }

    def _is_excluded(
        self, group_or_project_name: str, exclude_rules: list[str]
    ) -> bool:
        """
        Check if a group or project name matches an exclude rule. Each rule is a regex
        that is applied to the name. If any rule matches, the group or project should
        be excluded.
        """
        for rule in exclude_rules:
            if re.search(rule, group_or_project_name) is not None:
                return True

        return False
