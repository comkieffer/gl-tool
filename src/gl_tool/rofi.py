import importlib.resources
import logging
import shlex
import subprocess
import sys
from collections.abc import Iterable

from . import resources


def rofi(
    action_list: Iterable[str], prompt: str, exit_on_failure: bool = True
) -> tuple[str, bool]:
    """
    Show the ``rofi`` pane with the selection of options passed in ``action_list``.

    Returns the selected action (if a selection was made).

    Args:
        action_list: The list of options to present to the user
        prompt: The explanatory presented to the user to help them choose

    Returns:
        A boolean indicating if a selection was made (``True`` if a selection was made,
        ``False`` if the user closed the window without making a selection) and the
        text of the selection (``''`` if no selection was made).
    """
    actions = "\n".join(action_list)

    logging.info("Showing rofi action picker ...")

    theme_ref = importlib.resources.files(resources) / "rounded-blue-dark.rasi"

    with importlib.resources.as_file(theme_ref) as theme_path:
        # Rofi options:
        # -markup-rows  Enable pango markup for items
        # -dpi 1
        # -i            Case insensitive action matching
        rofi_cmd = (
            f"echo -n {shlex.quote(actions)} | "
            f"  rofi -markup-rows -dpi 1 -theme {theme_path} -dmenu -i -p '{prompt}'"
        )

        # Suppress S602 (subprocess call with shell=True identified, security issue.)
        #   We need the `shell=True` since we're piping stuff around.
        #   Can be deprecated when / if we switch to rofi-script mode
        ret = subprocess.run(
            rofi_cmd, shell=True, text=True, capture_output=True
        )  # noqa: S602

    logging.debug(
        f"Rofi returned {ret.returncode}, stdout = '{ret.stdout}', "
        f"stderr='{ret.stderr}'"
    )

    if ret.returncode != 0:
        logging.debug("Failed command was:")
        logging.debug(rofi_cmd)

    if exit_on_failure and ret.returncode != 0:
        sys.exit(1)

    return ret.stdout.strip(), ret.returncode == 0
