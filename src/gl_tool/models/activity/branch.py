from pydantic import BaseModel

from .event import ActivityEvent


class BranchEvent(ActivityEvent):
    @property
    def branch(self) -> str:
        assert self.push_data
        return self.push_data.ref

    @property
    def commit_count(self) -> int:
        assert self.push_data
        return self.push_data.commit_count


class BranchActivity(BaseModel):
    pushed_new: BranchEvent | None
    pushed_to: list[BranchEvent] = []
    deleted: BranchEvent | None

    def add_event(self, event: BranchEvent) -> None:
        # We don't have a simple way of differentiating between branches with the same
        # name. If for example, we create a branch 'foo', delete it, push again and
        # delete it one more time, we will have two _delete_ events.
        # We could be a bit smarted about tracking this but since I don't really care
        # about _pushed new_ and _delete_ events, I'll just eat the previous event.
        match event.action_name:
            case "pushed new":
                self.pushed_new = event
            case "pushed to":
                self.pushed_to.append(event)
            case "deleted":
                self.deleted = event
            case _:
                raise ValueError(
                    f"Unknown action name '{event.action_name}' for branch events"
                )

    @property
    def branch(self) -> str:
        events = (self.pushed_new, *self.pushed_to, self.deleted)
        activity = [a for a in events if a is not None]
        if len(activity) > 0:
            return activity[0].branch

        raise RuntimeError("Cannot retrieve branch name without `BranchEvents`.")

    @property
    def commit_count(self) -> int:
        return sum(c.commit_count for c in self.pushed_to)
