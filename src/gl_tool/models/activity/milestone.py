import logging

from pydantic import BaseModel

from .event import ActivityEvent


class MilestoneEvent(ActivityEvent):
    web_url: str | None = None

    @property
    def title(self) -> str:
        assert self.target_title
        return self.target_title

    @property
    def milestone_number(self) -> int:
        assert self.target_iid is not None
        return self.target_iid


class MilestoneActivity(BaseModel):
    opened: MilestoneEvent | None
    closed: MilestoneEvent | None

    def add_event(self, event: MilestoneEvent) -> None:
        match event.action_name:
            case "opened":
                if self.opened is None:
                    self.opened = event
                elif self.opened.created_at < event.created_at:
                    self.opened = event

                # We already have an open event, if we don't have a close event, then
                # we don't care. We're going from open to open.
                if self.closed is None:
                    return

                # We also have a close event, let's see if it is older than the new
                # open event. If we close, then open, then the state is open. Otherwise,
                # it stays closed.
                if self.closed.created_at < event.created_at:
                    logging.debug("Re-opening previously closed milestone.")
                    self.closed = None

            case "closed":
                if not self.closed:
                    self.closed = event
                    return

                # If we close, then somebody re-opens, then we close again, we will only
                # see the close events. only overwrite it if the new event is newer.
                if self.closed.created_at < event.created_at:
                    self.closed = event
            case _:
                raise ValueError(
                    f"Unknown action name '{event.action_name}' for issue events"
                )

    @property
    def title(self) -> str:
        events = (self.opened, self.closed)
        activity = [a for a in events if a is not None]
        if len(activity) > 0:
            return activity[0].title

        raise RuntimeError(
            "Cannot retrieve a milestone title without any `MilestoneEvent`s."
        )
