import logging

from pydantic import BaseModel

from .event import ActivityEvent


class MergeRequestEvent(ActivityEvent):
    @property
    def title(self) -> str:
        assert self.target_title
        return self.target_title


class MergeRequestActivity(BaseModel):
    opened: MergeRequestEvent | None
    commented: list[MergeRequestEvent] = []
    approved: MergeRequestEvent | None
    accepted: MergeRequestEvent | None
    closed: MergeRequestEvent | None

    def add_event(self, event: MergeRequestEvent) -> None:
        match event.action_name:
            case "opened":
                if self.opened is None:
                    self.opened = event
                elif self.opened.created_at < event.created_at:
                    # Only keep the first open event.
                    # If a merge request is closed and re-opened we will have a second
                    # open event. The one we really care about is the original creating
                    # of the MR, so we discard subsequent open events.
                    self.opened = event

                # If we re-open a previously closed MR, remove the close event
                if self.closed:
                    if self.closed.created_at < event.created_at:
                        logging.debug("Re-opening previously closed merge request.")
                        self.closed = None

                if self.accepted:
                    if self.accepted.created_at < event.created_at:
                        logging.debug("Re-opening previously accepted merge request.")
                        self.accepted = None

            case "commented on":
                self.commented.append(event)
            case "approved":
                assert self.approved is None
                self.approved = event
            case "accepted":
                assert self.accepted is None
                if self.closed is not None:
                    logging.debug(
                        "Clearing 'Closed' state on merge request since it was "
                        "'Accepted'"
                    )
                    self.closed = None
                self.accepted = event
            case "closed":
                assert self.closed is None
                # A merge request that has been closed can be re-opened and accepted
                # (i.e. merged). A merge request that has been merged cannot be
                # un-merged and closed.
                # For this reason, The "Accepted' state overrides the 'Closed' state.
                if self.accepted is not None:
                    logging.debug(
                        "Skipping event 'Closed' on merge request since it was "
                        "'Accepted'"
                    )
                    return

                self.closed = event
            case _:
                raise ValueError(
                    f"Unknown action name '{event.action_name}' for merge request event"
                )

    @property
    def title(self) -> str:
        events = (self.opened, *self.commented, self.approved, self.accepted, self.closed)
        activity = [a for a in events if a is not None]
        if len(activity) > 0:
            return activity[0].title

        raise RuntimeError(
            "Cannot retrieve merge request title without `MergeRequestEvents`."
        )
