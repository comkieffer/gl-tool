from __future__ import annotations

import re
from datetime import datetime
from typing import TYPE_CHECKING

from pydantic import BaseModel

if TYPE_CHECKING:
    from collections.abc import Callable, Iterator


class CommitHash(str):
    @classmethod
    def __get_validators__(cls) -> Iterator[Callable[[str], CommitHash]]:
        yield cls.validate

    @classmethod
    def validate(cls, v: str | datetime) -> CommitHash:
        if not isinstance(v, str):
            raise TypeError("string required")

        m = re.fullmatch(r"\b([a-f0-9]{7,40})\b", v)
        if not m:
            raise ValueError("invalid commit sha1 hash format")

        return cls(v)


class PushData(BaseModel):
    action: str

    commit_count: int
    commit_from: CommitHash | None
    commit_to: CommitHash | None
    commit_title: str | None

    ref: str
    ref_count: int | None
    ref_type: str


class NoteData(BaseModel):
    body: str
    noteable_iid: int


class ActivityEvent(BaseModel):
    action_name: str
    created_at: datetime

    target_id: int | None
    target_iid: int | None
    target_title: str | None
    target_type: str | None

    push_data: PushData | None
    note: NoteData | None
