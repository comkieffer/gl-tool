from gl_tool.cache import Cache

from .branch import BranchActivity, BranchEvent
from .issue import IssueActivity, IssueEvent
from .merge_request import MergeRequestActivity, MergeRequestEvent
from .milestone import MilestoneActivity, MilestoneEvent


class ProjectActivity:
    def __init__(self) -> None:
        # Active branches, key is the gitlab branch id
        self.branches: dict[str, BranchActivity] = {}
        self.merge_requests: dict[int, MergeRequestActivity] = {}
        self.milestones: dict[int, MilestoneActivity] = {}
        self.issues: dict[int, IssueActivity] = {}

    def hydrate(self, cache: Cache) -> None:
        pass

    def add_branch_event(self, e: BranchEvent) -> None:
        br = self.branches.setdefault(e.branch, BranchActivity())
        br.add_event(e)

    def add_merge_request_event(self, e: MergeRequestEvent) -> None:
        assert e.target_iid is not None
        mr = self.merge_requests.setdefault(e.target_iid, MergeRequestActivity())
        mr.add_event(e)

    def add_milestone_event(self, e: MilestoneEvent) -> None:
        assert e.target_iid is not None
        ms = self.milestones.setdefault(e.target_iid, MilestoneActivity())
        ms.add_event(e)

    def add_issue_event(self, e: IssueEvent) -> None:
        issue = self.issues.setdefault(e.issue_number, IssueActivity())
        issue.add_event(e)
