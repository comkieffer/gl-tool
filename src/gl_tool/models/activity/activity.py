from __future__ import annotations

from datetime import datetime
from pprint import pprint

from gl_tool.cache import Cache

from gitlab import Gitlab

from .branch import BranchEvent
from .issue import IssueEvent
from .merge_request import MergeRequestEvent
from .milestone import MilestoneEvent
from .project import ProjectActivity


class UserActivities:
    def __init__(self) -> None:
        self.projects: dict[int, ProjectActivity] = {}

    def hydrate(self, cache: Cache) -> None:
        for pr in self.projects.values():
            pr.hydrate(cache)

    @staticmethod
    def get(
        gl: Gitlab, cache: Cache, start_date: datetime, end_date: datetime
    ) -> UserActivities:
        activities = UserActivities()
        user_events = gl.events.list(
            iterator=True,
            after=start_date.strftime("%Y-%m-%d"),
            before=end_date.strftime("%Y-%m-%d"),
        )
        for event in user_events:
            if event.project_id not in activities.projects:
                activities.projects[event.project_id] = ProjectActivity()

            project = activities.projects[event.project_id]
            # See: https://docs.gitlab.com/ee/user/profile/contributions_calendar.html#user-contribution-events # noqa: E501
            # See: https://docs.gitlab.com/ee/api/events.html#target-types
            match event.target_type:
                case None:
                    # The created event has no useful data, it doesn't even have a
                    # branch name ...
                    if event.action_name == "created":
                        continue
                    project.add_branch_event(BranchEvent(**event.asdict()))
                case "MergeRequest":
                    project.add_merge_request_event(MergeRequestEvent(**event.asdict()))
                case "Milestone":
                    project.add_milestone_event(MilestoneEvent(**event.asdict()))
                case "Issue":
                    assert event.action_name in ["opened", "closed"]
                    project.add_issue_event(IssueEvent(**event.asdict()))
                # A `Note` is a comment on the merge request,
                # A `DiffNote` is a comment on a diff in the merge request
                case "Note" | "DiffNote" | "DiscussionNote":
                    assert event.action_name == "commented on"
                    match event.note["noteable_type"]:
                        case "Issue":
                            project.add_issue_event(IssueEvent(**event.asdict()))
                        case "MergeRequest":
                            project.add_merge_request_event(
                                MergeRequestEvent(**event.asdict())
                            )
                        case _:
                            raise RuntimeError(
                                f"Unknown value '{event.note['noteable_type']}' for "
                                "'noteable_type'."
                            )
                case _:
                    print("----------------")
                    print("New unknown event:")
                    print(f"  Action name: {event.action_name}")
                    print(f"  Target type: {event.target_type}")
                    print("----------------")
                    pprint(event.asdict())
                    print("----------------")
                    print("Press ENTER to continue ...")
                    input()

        return activities
