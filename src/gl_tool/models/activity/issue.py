import logging

from pydantic import BaseModel

from .event import ActivityEvent


class IssueEvent(ActivityEvent):
    web_url: str | None = None

    @property
    def title(self) -> str:
        assert self.target_title
        return self.target_title

    @property
    def issue_number(self) -> int:
        # For issue open/close events, the target of the event is the actual issue. For
        # commented_on events, the target of the id is the actual comment, the actual
        # issue id is in the `note` metadata.
        if self.action_name == "commented on":
            assert self.note is not None
            assert self.note.noteable_iid is not None
            return self.note.noteable_iid

        assert self.target_iid is not None
        return self.target_iid


class IssueActivity(BaseModel):
    opened: IssueEvent | None
    commented: list[IssueEvent] = []
    closed: IssueEvent | None

    def add_event(self, event: IssueEvent) -> None:
        match event.action_name:
            case "opened":
                if self.opened is None:
                    self.opened = event
                elif self.opened.created_at < event.created_at:
                    self.opened = event

                # We already have an open event, if we don't have a close event, then
                # we don't care. We're going from open to open.
                if self.closed is None:
                    return

                # We also have a close event, let's see if it is older than the new
                # open event. If we close, then open, then the state is open. Otherwise,
                # it stays closed.
                if self.closed.created_at < event.created_at:
                    logging.debug("Re-opening previously closed issue.")
                    self.closed = None
            case "commented on":
                self.commented.append(event)
            case "closed":
                if not self.closed:
                    self.closed = event
                    return

                # If we close, then somebody re-opens, then we close again, we will only
                # see the close events. only overwrite it if the new event is newer.
                if self.closed.created_at < event.created_at:
                    self.closed = event
            case _:
                raise ValueError(
                    f"Unknown action name '{event.action_name}' for issue events"
                )

    @property
    def title(self) -> str:
        events = (self.opened, *self.commented, self.closed)
        activity = [a for a in events if a is not None]
        if len(activity) > 0:
            return activity[0].title

        raise RuntimeError("Cannot retrieve an issue title without any `IssueEvent`s.")
