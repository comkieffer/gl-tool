from pydantic import BaseModel


class UserModel(BaseModel):
    id: int
    username: str
    name: str
    state: str
    avatar_url: str | None
    web_url: str
