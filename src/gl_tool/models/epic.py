import datetime
from typing import Literal

from pydantic import BaseModel


class EpicModel(BaseModel):
    id: int
    iid: int
    group_id: int

    # For chile epics, what is their parent ?
    parent_id: int | None

    title: str
    description: str
    state: Literal["active", "opened", "closed"]
    web_url: str

    start_date_is_fixed: bool | None
    start_date_fixed: datetime.date | None
    start_date_from_inherited_source: datetime.date | None

    due_date_is_fixed: bool | None
    due_date_fixed: datetime.date | None
    due_date_from_inherited_source: datetime.date | None
