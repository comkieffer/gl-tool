import datetime

from pydantic import BaseModel

from .epic import EpicModel
from .milestone import MilestoneModel


class GroupIteration(BaseModel):
    id: int
    group_id: int

    sequence: int
    title: str | None
    description: str | None
    web_url: str
    state: int
    start_date: datetime.date
    due_date: datetime.date


class ProjectReference(BaseModel):
    id: int


class GroupReference(BaseModel):
    id: int


class GroupModel(BaseModel):
    id: int
    parent_id: int | None
    description: str
    name: str
    full_name: str
    path: str
    full_path: str
    web_url: str
    avatar_url: str | None

    projects: list[ProjectReference]
    shared_projects: list[ProjectReference]

    subgroups: list[GroupReference] = []

    iterations: list[GroupIteration] | None
    milestones: dict[int, MilestoneModel] | None
    epics: dict[int, EpicModel] | None

    last_activity_at: datetime.datetime | None

    @property
    def activity_url(self) -> str:
        return self.web_url + "/-/activity"

    @property
    def members_url(self) -> str:
        return self.web_url + "/-/group_members"

    @property
    def epics_roadmap_url(self) -> str:
        return self.web_url + "/-/roadmap"

    @property
    def issue_list_url(self) -> str:
        return self.web_url + "/-/issues"

    @property
    def issue_board_url(self) -> str:
        return self.web_url + "/-/board"

    @property
    def milestones_url(self) -> str:
        return self.web_url + "/-/milestones"

    @property
    def iterations_url(self) -> str:
        return self.web_url + "/-/iterations"

    @property
    def current_iteration(self) -> GroupIteration | None:
        if self.iterations is None:
            return None

        today = datetime.date.today()
        for iteration in self.iterations:
            if iteration.start_date <= today <= iteration.due_date:
                return iteration

        return None
