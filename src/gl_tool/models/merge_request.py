import datetime

from pydantic import BaseModel

from .user import UserModel


class MergeRequestModel(BaseModel):
    id: int
    iid: int

    title: str
    description: str
    reference: str
    draft: bool
    web_url: str

    author: UserModel
    assignees: list[UserModel]
    reviewers: list[UserModel]

    project_id: int
    target_branch: str
    target_project_id: int

    created_at: datetime.datetime
    updated_at: datetime.datetime
    closed_at: datetime.datetime | None
    closed_by: UserModel | None
