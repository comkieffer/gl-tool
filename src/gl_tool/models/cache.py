from pathlib import Path

from pydantic import BaseModel

from .group import GroupModel
from .project import ProjectModel


class GroupsCache(BaseModel):
    groups: dict[int, GroupModel] | None


class ProjectsCache(BaseModel):
    projects: dict[int, ProjectModel] | None


class LocalProjectCache(BaseModel):
    """
    Base model for the cache of project locations used in the ``open_project`` command.
    """

    paths: list[Path]
