from typing import Literal

from pydantic import BaseModel

from .merge_request import MergeRequestModel
from .milestone import MilestoneModel

AccessLevel = Literal["disabled", "private", "public", "enabled"]


class ProjectModel(BaseModel):
    id: int | None
    description: str | None
    name: str
    name_with_namespace: str
    path: str
    path_with_namespace: str
    tag_list: list[str]

    archived: bool

    # Will be empty for projects that do not have the repository enabled
    default_branch: str | None

    milestones: dict[int, MilestoneModel] | None
    merge_requests: dict[int, MergeRequestModel] | None

    # Project Urls
    ssh_url_to_repo: str
    http_url_to_repo: str
    web_url: str
    readme_url: str | None
    avatar_url: str | None

    def branch_url(self, branch_name: str) -> str:
        return f"{self.web_url}/-/tree/{branch_name}"

    def merge_request_url(self, mr_iid: int) -> str | None:
        if self.merge_requests_url is None:
            return None

        assert self.merge_requests_url is not None
        return self.merge_requests_url + "/" + str(mr_iid)

    def issue_link(self, issue_number: int) -> str | None:
        if self.issue_list_url is None:
            return None

        assert self.issue_list_url is not None
        return self.issue_list_url + "/" + str(issue_number)

    def milestone_link(self, milestone_number: int) -> str | None:
        if self.milestones_url is None:
            return None

        return self.milestones_url + "/" + str(milestone_number)

    @property
    def activity_url(self) -> str:
        return self.web_url + "/-/activity"

    @property
    def members_url(self) -> str:
        return self.web_url + "/-/project_members"

    @property
    def branches_url(self) -> str:
        return self.web_url + "/-/branches"

    @property
    def tags_url(self) -> str:
        return self.web_url + "/-/tags"

    @property
    def issue_list_url(self) -> str | None:
        if self.__is_enabled_level(self.issues_access_level):
            return self.web_url + "/-/issues"
        return None

    @property
    def new_issue_url(self) -> str | None:
        if self.__is_enabled_level(self.issues_access_level):
            return self.web_url + "/-/issues/new"
        return None

    @property
    def issue_board_url(self) -> str | None:
        if self.__is_enabled_level(self.issues_access_level):
            return self.web_url + "/-/boards"
        return None

    @property
    def milestones_url(self) -> str | None:
        if self.__is_enabled_level(self.issues_access_level):
            return self.web_url + "/-/milestones"
        return None

    @property
    def merge_requests_url(self) -> str | None:
        if self.__is_enabled_level(self.merge_requests_access_level):
            return self.web_url + "/-/merge_requests"
        return None

    @property
    def new_merge_request_url(self) -> str | None:
        if self.__is_enabled_level(self.merge_requests_access_level):
            return self.web_url + "/-/merge_requests/new"
        return None

    @property
    def package_registry_url(self) -> str | None:
        if self.packages_enabled:
            return self.web_url + "/-/packages"
        return None

    @property
    def container_registry_url(self) -> str | None:
        if self.__is_enabled_level(self.container_registry_access_level):
            return self.web_url + "/-/container_registry"
        return None

    @property
    def pipelines_url(self) -> str | None:
        if self.__is_enabled_level(self.builds_access_level):
            return self.web_url + "/-/pipelines"
        return None

    @property
    def releases_url(self) -> str | None:
        if self.__is_enabled_level(self.releases_access_level):
            return self.web_url + "/-/releases"
        return None

    @property
    def settings_url(self) -> str:
        return self.web_url + "/edit"

    # The `package_registry_access_level` field is currently not enabled by default. We
    # still need the old field until it is.
    packages_enabled: bool | None

    analytics_access_level: AccessLevel
    builds_access_level: AccessLevel
    container_registry_access_level: AccessLevel
    environments_access_level: AccessLevel
    feature_flags_access_level: AccessLevel
    forking_access_level: AccessLevel
    infrastructure_access_level: AccessLevel
    issues_access_level: AccessLevel
    merge_requests_access_level: AccessLevel
    monitor_access_level: AccessLevel
    pages_access_level: AccessLevel
    releases_access_level: AccessLevel
    repository_access_level: AccessLevel
    requirements_access_level: AccessLevel
    security_and_compliance_access_level: AccessLevel
    snippets_access_level: AccessLevel
    wiki_access_level: AccessLevel

    # Project Information
    open_issues_count: int

    def __is_enabled_level(self, feature: AccessLevel) -> bool:
        return feature != "disabled"
