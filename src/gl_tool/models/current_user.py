from urllib.parse import urlparse

from pydantic import BaseModel


class CurrentUserModel(BaseModel):
    id: int
    username: str
    name: str
    web_url: str

    @property
    def activity_url(self) -> str:
        url = urlparse(self.web_url)
        return f"{url.scheme}://{url.netloc}/users/{self.username}/activity"

    @property
    def assigned_issues_url(self) -> str:
        url = urlparse(self.web_url)
        return (
            f"{url.scheme}://{url.netloc}/dashboard/"
            f"issues?assignee_username={self.username}"
        )

    @property
    def assigned_merge_requests_url(self) -> str:
        url = urlparse(self.web_url)
        return (
            f"{url.scheme}://{url.netloc}/dashboard/merge_requests"
            f"?assignee_username={self.username}"
        )

    @property
    def review_requests_url(self) -> str:
        url = urlparse(self.web_url)
        return (
            f"{url.scheme}://{url.netloc}/dashboard/merge_requests"
            f"?reviewer_username={self.username}"
        )
