import datetime
from typing import Literal

from pydantic import BaseModel


class MilestoneModel(BaseModel):
    id: int
    iid: int

    # Id of the project for which the milestone was created.
    # `None` for group milestones.
    project_id: int | None

    # Id of the group for which the milestone was created.
    # `None` for project milestones.
    group_id: int | None

    title: str
    description: str
    state: Literal["active", "closed"]

    due_date: datetime.date | None
    start_date: datetime.date | None

    @property
    def is_group_milestone(self) -> bool:
        return self.group_id is not None

    @property
    def is_project_milestone(self) -> bool:
        return self.project_id is not None

    @property
    def expired(self) -> bool:
        return self.due_date is not None and self.due_date < datetime.date.today()

    @property
    def active(self) -> bool:
        return self.state == "active"
