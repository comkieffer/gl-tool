import logging
import shutil
import sys
from functools import cached_property
from pathlib import Path

import gitlab

from platformdirs import user_cache_path
from pydantic import ValidationError

from . import __app_slug__
from .cache_refresher import CacheRefresher, CacheRefreshOpts
from .models import (
    CurrentUserModel,
    GroupModel,
    GroupsCache,
    ProjectModel,
    ProjectsCache,
)


class Cache:
    def __init__(self, load: bool = True) -> None:
        self.user_cache_file = self.cache_dir / "user.json"
        self.groups_cache_file = self.cache_dir / "groups.json"
        self.projects_cache_file = self.cache_dir / "projects.json"

        self.user: CurrentUserModel | None = None
        self.groups: dict[int, GroupModel] = {}
        self.projects: dict[int, ProjectModel] = {}

        if load:
            try:
                self._load()
            except ValidationError as exc:
                logging.error("Unable to load cache. Validation failed.")
                logging.error(f"Rebuild the cache with '{__app_slug__} cache refresh'")
                logging.debug(f"Validation error was: {exc}")
                sys.exit(10)

    def _load(self) -> None:
        # if we don't have a cache directory, there isn't much that we can do.
        if not self.cache_dir.is_dir():
            logging.warning(
                f"Cache directory {self.cache_dir} does not exist. Populate it with "
                f"`{__app_slug__} cache refresh`."
            )
            return

        logging.info("Loading cache from disk ...")
        self._load_user()
        self._load_groups()
        self._load_projects()

    def _load_user(self) -> None:
        if not self.user_cache_file.is_file():
            logging.warning(f"User cache file {self.user_cache_file} does not exist")
            return

        cached_user = CurrentUserModel.parse_file(self.user_cache_file)
        if cached_user is not None:
            self.user = cached_user

    def _load_groups(self) -> None:
        if not self.groups_cache_file.is_file():
            logging.warning(
                f"Groups cache file {self.groups_cache_file} does not exist"
            )
            return

        cached_groups = GroupsCache.parse_file(self.groups_cache_file)
        if cached_groups.groups is not None:
            self.groups = cached_groups.groups

    def _load_projects(self) -> None:
        if not self.projects_cache_file.is_file():
            logging.warning(
                f"Projects cache file {self.projects_cache_file} does not exist"
            )
            sys.exit(1)

        cached_projects = ProjectsCache.parse_file(self.projects_cache_file)
        if cached_projects.projects is not None:
            self.projects = cached_projects.projects

    def refresh(
        self, gl: gitlab.Gitlab, max_parallel_jobs: int, opts: CacheRefreshOpts
    ) -> bool:
        logging.info("Refreshing cache ...")
        self.clear()

        if not self.cache_dir.exists():
            self.cache_dir.mkdir()

        refresher = CacheRefresher()
        if not refresher.refresh(gl, max_parallel_jobs, opts):
            logging.error("Unable to refresh cache.")
            return False

        self.user = refresher.user
        self.projects = refresher.projects
        self.groups = refresher.groups

        logging.info("Cache refreshed successfully.")
        return True

    def write(self) -> None:
        """Write to cache to disk."""
        if self.user is None:
            return

        with self.user_cache_file.open("w+") as f:
            f.write(self.user.json(indent=4))

        groups_cache = GroupsCache()
        groups_cache.groups = self.groups
        with self.groups_cache_file.open("w+") as f:
            f.write(groups_cache.json(indent=4))

        projects_cache = ProjectsCache()
        projects_cache.projects = self.projects
        with self.projects_cache_file.open("w+") as f:
            f.write(projects_cache.json(indent=4))

    def clear(self, remove: bool = False) -> None:
        """
        Reset the cache to a clean state (empty it).

        Optionally, delete the cache folder on disk.

        Args:
            remove (bool, optional): Should the on-disk cache folder be deleted.
                Defaults to False.
        """
        if remove and self.cache_dir.is_dir():
            logging.warning(f"Removing cache directory {self.cache_dir} ...")
            shutil.rmtree(self.cache_dir)

        logging.debug("Clearing existing cache ...")
        self.user = None
        self.groups = {}
        self.projects = {}

    def children_of(self, group_id: int) -> list[ProjectModel]:
        if group_id not in self.groups:
            raise KeyError(group_id)

        projects = []
        project_refs = self.groups[group_id].projects
        for project_ref in project_refs:
            try:
                projects.append(self.projects[project_ref.id])
            except KeyError as exc:
                logging.warning(f"Unable to locate project with id {exc}.")
                logging.warning("Repos file may be incomplete.")
                continue

        subgroups = [*self.groups[group_id].subgroups]
        for subgroup_ref in subgroups:
            projects.extend(self.children_of(subgroup_ref.id))

        return projects

    @cached_property
    def cache_dir(self) -> Path:
        return user_cache_path() / __app_slug__

    @property
    def is_empty(self) -> bool:
        return len(self.groups) == 0 and len(self.projects) == 0
