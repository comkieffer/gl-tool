#!/usr/bin/env python3

import logging

from gl_tool import __app_slug__
from gl_tool.models import LocalProjectCache

from platformdirs import user_cache_path
from rofi_menu import Item, Menu, ShellItem, run  # type: ignore


def rofi_error(error_msg: str) -> None:
    run(Menu(items=[Item(error_msg)], prompt="An error occurred!"))


def main() -> None:
    """
    This is the entrypoint called by rofi.

    - Rofi calls the executable without arguments on startup.
    - If the user selects an option, rofi calls the executable with the text of that
      option as the first argument.
    - If the script returns no entries, rofi quits.

    On the first run, we generate the list of entries. On subsequent runs we skip this
    step and only populate the menu with the required entry (the first argument passed
    to the script by rofi).
    """

    projects_cache = user_cache_path() / __app_slug__ / "local_project_path.json"
    logging.info(f"Loading local projects cache from {projects_cache}")

    projects = LocalProjectCache.parse_file(projects_cache)
    if len(projects.paths) == 0:
        rofi_error(
            f"The projects cache file ({projects_cache}) was empty. Run "
            f"'{__app_slug__} open_project --refresh' to re-populate it."
        )

    run(
        Menu(
            items=[
                ShellItem(text=str(f"{path.stem:30} ({path})"), command=f"code {path}")
                for path in projects.paths
            ],
            prompt="Select project to open",
        ),
        rofi_version="1.6",
    )


if __name__ == "__main__":
    main()
