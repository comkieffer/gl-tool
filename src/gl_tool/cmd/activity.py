import datetime
import logging
import sys

import gitlab

import click
from rich import print

from .. import __app_slug__
from ..models.activity import UserActivities
from .types import HumanDate


@click.command()
@click.option("-f", "--from", "start_date", type=HumanDate(), default="today")
@click.option("-t", "--to", "end_date", type=HumanDate())
@click.pass_context
def activity_command(
    ctx: click.Context,
    start_date: datetime.datetime,
    end_date: datetime.datetime | None = None,
) -> None:
    config = ctx.obj["config"]
    cache = ctx.obj["cache"]

    if end_date is None:
        end_date = datetime.datetime.today() + datetime.timedelta(days=1)

    try:
        gl = gitlab.Gitlab(
            url=config.get("url"),
            private_token=config.get("token"),
        )
    except ValueError as exc:
        logging.error(
            f"<{exc}> not set. Set it with '{__app_slug__} config set {exc} VALUE' "
        )
        sys.exit(1)

    print(
        f"Retrieving events from {start_date.strftime('%Y-%m-%d')} to "
        f"{end_date.strftime('%Y-%m-%d')} ..."
    )

    user_activity = UserActivities.get(
        gl, cache, start_date=start_date, end_date=end_date
    )

    for pid, project_activities in user_activity.projects.items():
        if pid not in cache.projects:
            print(
                f"Skipping project #{pid} since it is not in the cache.\n"
                f"Refresh the cache with '{__app_slug__} cache refresh' to suppress "
                "this warning."
            )
            continue

        project = cache.projects[pid]
        print(f"On [bold]{project.name_with_namespace}[/bold]")

        for br in project_activities.branches.values():
            # We only really care about branches with activity
            # If the branch has been deleted then either the commits were merged (and
            # they will show up in that branches activity) or we pushed shit and we
            # should ignore it.
            if len(br.pushed_to) == 0 or br.deleted is not None:
                continue

            branch_url = project.branch_url(br.branch)
            print(
                f"  - Pushed {br.commit_count} commits to "
                f"[link={branch_url}]{br.branch}[/link]"
            )

        # we should find a way to associate pushes to branches with merge requests so
        # that we can fuse them into a single entry
        for mr in project_activities.merge_requests.values():
            # Only look at code activities for now, we'll handle reviews later
            mr_events = (mr.opened, *mr.commented, mr.approved, mr.accepted, mr.closed)
            mr_activity = [a for a in mr_events if a is not None]
            if len(mr_activity) == 0:
                continue

            mr_url = project.merge_request_url(mr_activity[0].target_iid)
            mr_link = f"[link={mr_url}]{mr.title}[/link]"
            if mr.opened:
                print(f"  - Created new merge request {mr_link}")
            if mr.accepted:
                print(f"  - Accepted {mr_link}")
            if mr.closed:
                print(f"  - Declined {mr_link}")

        for ms in project_activities.milestones.values():
            ms_events = (ms.opened, ms.closed)
            ms_activity = [m for m in ms_events if m is not None]
            if len(ms_activity) == 0:
                continue

            ms_url = project.milestone_link(ms_activity[0].milestone_number)
            ms_link = f"[link={ms_url}]{ms.title}[/link]"
            if ms.opened:
                print(f"  - Opened milestone - {ms_link}")
            if ms.closed:
                print(f"  - Closed milestone - {ms_link}")

        for iss in project_activities.issues.values():
            iss_events = (iss.opened, *iss.commented, iss.closed)
            iss_activity = [a for a in iss_events if a is not None]
            if len(iss_activity) == 0:
                continue

            iss_url = project.issue_link(iss_activity[0].issue_number)
            iss_number = iss_activity[0].issue_number
            iss_link = f"[link={iss_url}]{iss.title}[/link] (#{iss_number})"

            # only show the most important event.
            # Closing > Opening > Commenting
            if iss.closed:
                print(f"  - Closed issue - {iss_link}")
            elif iss.opened:
                print(f"  - Opened issue - {iss_link}")
            else:
                assert len(iss.commented) > 0
                print(f"  - Commented on - {iss_link}")

        print()
