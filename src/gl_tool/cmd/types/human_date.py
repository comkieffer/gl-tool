import datetime

import parsedatetime  # type: ignore
from click import Context, Parameter, ParamType


class HumanDate(ParamType):
    name = "date"

    def convert(
        self,
        value: str | datetime.datetime,
        param: Parameter | None,
        ctx: Context | None,
    ) -> datetime.datetime | None:
        if isinstance(value, datetime.datetime):
            return value

        cal = self.__get_pdt_calendar()
        date, flag = cal.parse(value)
        if flag == 0:
            assert param
            self.fail(f"invalid date: {value} for {param.name}")
            return None

        # Only retrieve the year, month and day parts of the date
        return datetime.datetime(*date[:3])

    @classmethod
    def __get_pdt_calendar(cls) -> parsedatetime.Calendar:
        consts = parsedatetime.Constants(usePyICU=False)
        # "Monday" will be either today or the last Monday
        consts.DOWParseStyle = -1
        return parsedatetime.Calendar(consts)
