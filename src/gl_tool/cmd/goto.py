import logging
import re
import subprocess
from dataclasses import dataclass
from textwrap import shorten
from typing import Any

import click

from ..cache import Cache
from ..models import GroupModel, ProjectModel
from ..rofi import rofi


class ActionPicker:
    pass


@dataclass
class OpenMergeRequestPicker(ActionPicker):
    project: ProjectModel


@dataclass
class OpenIssuePicker(ActionPicker):
    project: ProjectModel


@click.command()
@click.option("-i", "--include-archived", help="Include archived repositories")
@click.pass_context
def goto_command(ctx: click.Context, include_archived: bool) -> None:
    """
    Select a group or repo to open with Rofi.

    Long names can be transformed by adding a replacement rule. See 'gl-tool config
    add-replacement --help' for more details.
    """
    cache = ctx.obj["cache"]
    replacement_rules = ctx.obj["config"].get("replacements")

    # Show the picker with the list of groups, projects and user actions
    available_actions: dict[str, Any] = prepare_root_menu(
        cache, include_archived, replacement_rules
    )
    selected_option, _ = rofi(
        sorted(available_actions.keys()), "Pick the group to open", exit_on_failure=True
    )
    selected_action = available_actions[selected_option]

    # If the action is just a string, it is an url that we should open now.
    if isinstance(selected_action, str):
        logging.info(f"Opening {selected_action} ...")
        subprocess.run(["/usr/bin/env", "xdg-open", selected_action], check=True)
        return

    # Otherwise, the action is a group or repository
    logging.info(f"Preparing menu for {selected_option} ...")
    group_or_repo = selected_action
    assert isinstance(group_or_repo, (GroupModel, ProjectModel))

    available_actions = {
        "Overview": group_or_repo.web_url,
        "Activity": group_or_repo.activity_url,
        "Members": group_or_repo.members_url,
    }

    if isinstance(group_or_repo, GroupModel):
        available_actions.update(prepare_group_menu(group_or_repo))
    elif isinstance(group_or_repo, ProjectModel):
        available_actions.update(prepare_project_menu(group_or_repo))
    else:
        raise RuntimeError("This should never happen")

    selected_option, _ = rofi(
        available_actions.keys(), "Pick the page to open", exit_on_failure=True
    )
    selected_action = available_actions[selected_option]

    # if the result is an url, we just open it, otherwise, we dispatch
    if isinstance(selected_action, str):
        logging.info(f"Opening {selected_action} ...")
        subprocess.run(["/usr/bin/env", "xdg-open", selected_action], check=True)
        return

    if isinstance(selected_action, OpenMergeRequestPicker):
        logging.info("Preparing merge request menu ...")
        available_actions = prepare_merge_request_menu(selected_action.project)
    elif isinstance(selected_action, OpenIssuePicker):
        logging.info("Preparing issue menu ...")
        available_actions = prepare_issues_menu(selected_action.project)
    else:
        raise RuntimeError("This should never happen")

    selected_option, _ = rofi(
        available_actions.keys(), "Pick the merge request to open", exit_on_failure=True
    )
    selected_action = available_actions[selected_option]

    # If the action is just a string, it is an url that we should open now.
    if isinstance(selected_action, str):
        logging.info(f"Opening {selected_action} ...")
        subprocess.run(["/usr/bin/env", "xdg-open", selected_action], check=True)
        return


def apply_name_replacements(name: str, replacement_rules: list[dict[str, str]]) -> str:
    for rule in replacement_rules:
        name = re.sub(rule["pattern"], rule["replacement"], name)

    return name


def prepare_root_menu(
    cache: Cache, include_archived: bool, replacement_rules: list[dict[str, str]]
) -> dict[str, str | GroupModel | ProjectModel]:
    """
    Prepare the first menu: the list of quick links, groups and projects that the user
    can jump to.

    Returns:
        A dictionary where the key is a _name_ for the action, the actual text of that
        will be shown to the user and the value is a Python object that will be used to
        generate the next menu.
    """

    # Add actions related to the current user (Your issues, Your MRs, ...)
    assert cache.user is not None
    available_actions: dict[str, str | GroupModel | ProjectModel] = {
        "Your Activity": cache.user.activity_url,
        "Issues assigned to you": cache.user.assigned_issues_url,
        "Merge requests assigned to you": cache.user.assigned_merge_requests_url,
        "Review requests for you": cache.user.review_requests_url,
    }

    # Add links to accessible Groups
    for grp in cache.groups.values():
        grp_key = (
            f'<span size="small">(G)</span> '
            f"{apply_name_replacements(grp.full_name, replacement_rules)}"
        )
        available_actions[grp_key] = grp

    # Add links to accessible projects
    for proj in cache.projects.values():
        if not include_archived and proj.archived:
            continue

        proj_key = (
            f'<span size="small">(P)</span>: '
            f"{apply_name_replacements(proj.name_with_namespace, replacement_rules)}"
        )
        available_actions[proj_key] = proj

    return available_actions


def prepare_group_menu(
    selected_group: GroupModel,
) -> dict[str, str]:
    target_pages: dict[str, str] = {}

    if selected_group.epics is not None:
        target_pages["Epics Roadmap"] = selected_group.epics_roadmap_url

    target_pages["Issues"] = selected_group.issue_list_url
    target_pages["Issue Board"] = selected_group.issue_board_url

    if selected_group.milestones is not None:
        target_pages["Milestones"] = selected_group.milestones_url

    if selected_group.iterations is not None:
        target_pages["Iterations"] = selected_group.iterations_url

        current_iteration = selected_group.current_iteration
        if current_iteration is not None:
            target_pages["Current Iteration"] = current_iteration.web_url

    return target_pages


def prepare_project_menu(
    selected_project: ProjectModel,
) -> dict[str, str | ActionPicker]:
    target_pages: dict[str, str | ActionPicker] = {
        "Branches": selected_project.branches_url,
        "Tags": selected_project.tags_url,
        "Settings": selected_project.settings_url,
    }

    if selected_project.issue_list_url:
        target_pages["Issues"] = OpenIssuePicker(selected_project)

        if selected_project.milestones is not None:
            assert selected_project.milestones_url is not None
            target_pages["Milestones"] = selected_project.milestones_url

    if selected_project.merge_requests_url:
        target_pages["Merge Requests"] = OpenMergeRequestPicker(selected_project)

    if selected_project.package_registry_url:
        target_pages["Package registry"] = selected_project.package_registry_url

    if selected_project.container_registry_url:
        target_pages["Container Registry"] = selected_project.container_registry_url

    if selected_project.pipelines_url:
        target_pages["Pipelines"] = selected_project.pipelines_url

    if selected_project.releases_url:
        target_pages["Releases"] = selected_project.releases_url

    return target_pages


def prepare_issues_menu(project: ProjectModel) -> dict[str, str]:
    assert project.issues_access_level != "disabled"

    assert project.issue_list_url is not None
    assert project.issue_board_url is not None
    assert project.new_issue_url is not None

    return {
        "Issue List": project.issue_list_url,
        "Issue Board": project.issue_board_url,
        "Create New Issue": project.new_issue_url,
    }


def prepare_merge_request_menu(project: ProjectModel) -> dict[str, str]:
    assert project.merge_requests_url is not None
    assert project.new_merge_request_url is not None

    target_pages = {
        "List Merge Requests": project.merge_requests_url,
        "New Merge Request": project.new_merge_request_url,
    }

    if project.merge_requests is not None:
        for mr in project.merge_requests.values():
            target_pages[f"Open {mr.reference} '{shorten(mr.title, 70)}'"] = mr.web_url

    return target_pages
