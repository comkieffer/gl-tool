import logging
import re
import sys

import click

from ..errors import ReturnCode


@click.group(help="Get and set configuration options")
def config_command_group() -> None:
    # Do nothing, just print help
    pass


@config_command_group.command("set", help="Set the value of a configuration option")
@click.pass_context
@click.option(
    "-a",
    "--add",
    help="Append the value to a list-like configuration key instead of overwriting it.",
)
@click.argument("config-key")
@click.argument("config-value")
def config_set(
    ctx: click.Context, add: bool, config_key: str, config_value: str
) -> None:
    config = ctx.obj["config"]

    try:
        if add:
            if not config.add(config_key, config_value):
                sys.exit(ReturnCode.Failed)
        else:
            config.set(config_key, config_value)
    except KeyError as exc:
        # TODO: Error message should provide doc help
        logging.error(f"Unable to set <{exc}>. Not a valid configuration key.")
        sys.exit(ReturnCode.Failed)

    config.save()


@config_command_group.command("get", help="Set the value of a configuration option")
@click.pass_context
@click.argument("config-key")
def config_get(ctx: click.Context, config_key: str) -> None:
    config = ctx.obj["config"]

    try:
        click.echo(config.get(config_key))
    except KeyError as exc:
        # TODO: Error message should provide doc help
        logging.error(f"<{exc}> is not a valid configuration key.")
        sys.exit(ReturnCode.Failed)


@config_command_group.command()
@click.argument("pattern")
@click.argument("replacement")
@click.pass_context
def add_replacement(ctx: click.Context, pattern: str, replacement: str) -> None:
    """
    Save a new text replacement rule to the configuration.

    Store a (pattern, replacement) pair to use to replace parts of the name of a
    repository or group when shown in the `goto` view. The PATTERN is a regex which is
    used to replace the matched fragment of the input with the REPLACEMENT.

    Example:

        $ gl-tool config add-replacement '^Human Robot Interaction Lab /' 'HRI'

    Saves a regular expression which matches `Human Robot Interaction Lab` from the
    start of the line and replaces it with 'HRI' when showed in the `goto` menu.
    """
    config = ctx.obj["config"]

    try:
        re.compile(pattern)
    except re.error as exc:
        logging.error(f"Pattern is not a valid regular expression. Error was: {exc}")
        sys.exit(ReturnCode.Failed)

    logging.info(f"Adding new replacement pattern: '{pattern}' -> '{replacement}'")
    new_rule = {"pattern": pattern, "replacement": replacement}

    current_rules = config.get("replacements")
    if new_rule in current_rules:
        logging.info("Rule is already present. Skipping.")
        sys.exit(ReturnCode.Ok)

    config.add("replacements", new_rule)
    config.save()
