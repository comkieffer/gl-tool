import logging
import sys

import gitlab

import click
from requests.adapters import HTTPAdapter

from .. import __app_slug__
from ..cache_refresher import CacheRefreshOpts
from ..errors import ReturnCode


@click.group
def cache_command_group() -> None:
    """
    Manipulate the application cache (clear, refresh, ...).
    """
    pass


@cache_command_group.command("refresh")
@click.pass_context
def cache_refresh(ctx: click.Context) -> None:
    """
    Refresh the groups and projects cache.

    The cache contains the tree of all accessible groups and projects starting from
    the base 'group-id'. Once the 'group-id' is set once, it will be stored in the
    configuration file and used for future invocations.

    The group-id is used to narrow the search for visible repositories since otherwise
    every single public repository on the instance would be added to your cache.

    Args:
        ctx (Context): The click context object
    """
    config = ctx.obj["config"]

    try:
        gl = gitlab.Gitlab(
            url=config.get("url"),
            private_token=config.get("token"),
        )
    except ValueError as exc:
        logging.error(
            f"<{exc}> not set. Set it with '{__app_slug__} config set {exc} VALUE' "
        )
        sys.exit(ReturnCode.ConfigError)

    click.echo(f"Connecting to gitlab instance {config.get('url')} ... ")
    gl.auth()

    # Gitlab uses the `requests` library to do HTTP. In turn, `requests` uses the
    # `urllib3` library to actually perform the requests. The library recycles
    # connections in a `ConnectionPool` to avoid creating a new connection for each
    # request.
    # When we run multiple requests in parallel, we can use up all the connections in
    # the pool. New connections will be discarded when they are finished and a log
    # message will be emitted. To fix this, we need to increase the size of the
    # connection pool so that we never run out of connection
    adapter = HTTPAdapter(pool_connections=10, pool_maxsize=100)
    gl.session.mount("https://", adapter)

    # How many different threads should be used when refreshing the cache.
    max_parallel_jobs = int(config.get("cache-refresh-workers"))

    additional_opts = CacheRefreshOpts(
        exclude_rules=config.get("cache-refresh-excludes"),
        supports_epics=config.get("plan") != "free",
        supports_iterations=config.get("plan") != "free",
    )

    click.echo("Refreshing cache, this may take a few minutes.")
    cache = ctx.obj["cache"]
    if not cache.refresh(gl, max_parallel_jobs, additional_opts):
        sys.exit(ReturnCode.Failed)

    cache.write()

    click.echo(
        f"Cache contains {len(cache.groups)} groups and {len(cache.projects)} projects."
    )

    sys.exit(ReturnCode.Ok)


@cache_command_group.command("clear")
@click.pass_context
def cache_clear(ctx: click.Context) -> None:
    ctx.obj["cache"].clear(remove=True)
    sys.exit(ReturnCode.Ok)
