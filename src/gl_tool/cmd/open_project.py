#!/usr/bin/env python3

import logging
import re
import subprocess
from os.path import expanduser
from pathlib import Path

import click

from ..models import LocalProjectCache


def collect_projects() -> LocalProjectCache:
    ret = subprocess.run(
        ["fdfind", "--hidden", "^.git$", expanduser("~")],
        text=True,
        capture_output=True,
    )
    ret.check_returncode()

    subprocess.run("echo $(date) >> ~/gl-tool-rofi.log", shell=True)

    git_folders = ret.stdout.splitlines()
    projects = []
    exclude_patterns = [
        re.escape(expanduser("~/.")),
        re.escape(expanduser("/build/")),
    ]
    for folder in git_folders:
        for pat in exclude_patterns:
            # If the folder name matches one of the exclude patterns, ignore it
            if re.search(pat, folder):
                break
        else:
            projects.append(Path(folder).parent)

    return LocalProjectCache(paths=projects)


@click.command()
@click.option("-r", "--refresh", help="Refresh the list of known projects")
@click.pass_context
def open_project_command(ctx: click.Context, refresh: bool) -> None:
    cache_dir = ctx.obj["cache"].cache_dir
    projects_cache = cache_dir / "local_project_path.json"

    if refresh or not projects_cache.exists():
        logging.info("Updating local projects cache ...")
        projects = collect_projects()

        logging.info(f"Writing local projects cache to {projects_cache} ...")
        with projects_cache.open("w+") as f:
            f.write(projects.json())

    logging.info("Calling rofi modi ...")
    subprocess.run(
        [
            "rofi",
            "-modi",
            "main:src/gl_tool/rofi_entrypoints/open_project.py",
            "-show",
            "main",
        ]
    )
