import logging
import sys

import click
import yaml

from .. import __app_slug__


@click.command()
@click.option(
    "-c",
    "--children-of",
    required=True,
    type=int,
    help="Include all the child repositories of this group (id or name)",
)
@click.pass_context
def generate_repos_command(ctx: click.Context, children_of: str) -> None:
    """
    Generate a .repos file for vcs-tools and write to stdout.

    To generate a .repos file containing all projects in the cache, use

        $ gl-tool generate-repos > vcs.repos

    This can return a long list of results. To narrow it down, use `--children-of` to
    only include projects contained in the specified group.

        $ gl-tool generate-repos --children-of GROUP_ID > vcs.repos

    \f
    Args:
        ctx (Context): Click context object
    """

    cache = ctx.obj["cache"]

    if cache.is_empty:
        logging.error(
            f"Cache is empty. use '{__app_slug__} cache refresh' to re-populate it."
        )
        sys.exit(1)

    try:
        repos = cache.children_of(children_of)
    except KeyError:
        logging.error(
            f"Unable to locate a group with an id of {children_of} in the cache."
        )
        sys.exit(1)

    formatted_repos = {}
    for proj in repos:
        if proj.archived:
            continue

        repo_path_parts = proj.name_with_namespace.split(" / ")
        repo_path = "/".join(repo_path_parts[1:])
        formatted_repos[repo_path.strip()] = {
            "type": "git",
            "url": proj.http_url_to_repo,
            "version": proj.default_branch,
        }

    click.echo(yaml.dump({"repositories": formatted_repos}))
