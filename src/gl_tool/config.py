import logging
from collections.abc import Mapping, MutableMapping
from pathlib import Path
from typing import Any

import yaml

DEFAULT_SECTION = "global"

#TODO: replace the config parser with the pydantic config model

DEFAULT_CONFIG = {
    DEFAULT_SECTION: {
        "url": "",
        "token": "",
        "plan": "free",
        "replacements": [],
        # How many parallel jobs should be started when refreshing the cache.
        "cache-refresh-workers": 20,
        # A list of regexes applied to group or project full names. If the regex
        # matches, the group or project is ignored.
        "cache-refresh-excludes": [],
    }
}


def deep_update(
    d: MutableMapping[Any, Any], u: Mapping[Any, Any]
) -> MutableMapping[Any, Any]:
    for k, v in u.items():
        if isinstance(v, Mapping):
            d[k] = deep_update(d.get(k, {}), v)
        else:
            d[k] = v
    return d


class Config:
    def __init__(self, file: Path) -> None:
        self._config = DEFAULT_CONFIG
        self._filename = file

        if file.exists():
            with file.open() as cfg_f:
                logging.debug(f"Reading configuration from {self._filename}")
                deep_update(self._config, yaml.safe_load(cfg_f))
        else:
            logging.warning(f"Configuration file {self._filename} does not exist")

    # noqa: A003 (class attribute "set" is shadowing builtin)
    def set(self, key: str, value: Any) -> None:
        if key not in DEFAULT_CONFIG[DEFAULT_SECTION]:
            raise KeyError(key)

        self._config[DEFAULT_SECTION][key] = value

    def add(self, key: str, new_value: Any) -> bool:
        if key not in DEFAULT_CONFIG[DEFAULT_SECTION]:
            raise KeyError(key)

        config_value = self._config[DEFAULT_SECTION][key]
        if not isinstance(config_value, list):
            logging.error(
                f"Cannot append value to config option {key} which is a "
                f"{type(config_value).__name__}"
            )
            return False

        config_value.append(new_value)
        return True

    def get(self, key: str) -> Any:
        value = self._config[DEFAULT_SECTION][key]
        if value == "":
            raise ValueError(key)

        return value

    def save(self) -> None:
        logging.debug(f"Saving configuration to {self._filename}")

        with self._filename.open("w+") as f:
            f.write(yaml.safe_dump(self._config))
