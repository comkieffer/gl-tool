#! /usr/bin/env python3

import logging
import sys
from pathlib import Path

import click
from click import Context
from platformdirs import user_config_path, user_log_path
from rich.traceback import install

from . import __app_slug__
from .cache import Cache
from .cmd.activity import activity_command
from .cmd.cache import cache_command_group
from .cmd.config import config_command_group
from .cmd.generate_repos import generate_repos_command
from .cmd.goto import goto_command
from .cmd.open_project import open_project_command
from .config import Config

# Install the rich traeback handler. This ensures that all uncaught exceptions will be
# rendered with highlighting.
install(show_locals=True)


def initialise_logging(verbosity: int) -> None:
    log_file = user_log_path() / f"{__app_slug__}.log"

    # Ensure that the log path exists.
    # On Linux, `~/.local/state` is still uncommon, so we make sure that the directory
    # exists. We don't need to do this for other paths since they are quite well known
    # at this point.
    log_dir = log_file.parent
    log_dir.mkdir(exist_ok=True, parents=True)

    logging.basicConfig(
        filename=log_file,
        format="%(asctime)s: [%(levelname)7s] / %(name)s: %(message)s",
        level=logging.DEBUG,
        filemode="w+",
    )

    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(logging.Formatter("[%(levelname)7s] %(message)s"))

    match verbosity:
        case 0:
            stream_handler.setLevel(logging.WARNING)
        case 1:
            stream_handler.setLevel(logging.INFO)
        case _:
            stream_handler.setLevel(logging.DEBUG)

            # Connection pool is very verbose in debug mode.
            # The filehandler will log it to file, so we won't actually lose this
            # information if we need it. It just won't appear in the console where we
            # usually don't care about it.
            urllib_logger = logging.getLogger("urllib3.connectionpool")
            urllib_logger.setLevel(logging.INFO)

    logging.getLogger().addHandler(stream_handler)
    logging.info(f"Writing full log to {log_file}.")


@click.group()
@click.option(
    "-c",
    "--config-dir",
    type=click.Path(exists=True, file_okay=False, writable=True),
    help="Directory containing the configuration files",
)
@click.option(
    "-v",
    "--verbose",
    default=False,
    count=True,
    help="Enable verbose output",
)
@click.version_option()
@click.pass_context
def main(ctx: Context, config_dir: click.Path, verbose: int) -> None:
    ctx.ensure_object(dict)
    initialise_logging(verbose)

    ctx.obj["config-dir"] = user_config_path() / __app_slug__
    if config_dir is not None:
        ctx.obj["config-dir"] = Path(str(config_dir))

    ctx.obj["config-file"] = ctx.obj["config-dir"] / "config.yaml"
    ctx.obj["config"] = Config(ctx.obj["config-file"])

    # Don't automatically load the cache from file if we're calling `cache` subcommands
    # since we're either refreshing it or clearing it. In both cases we don't need it.
    ctx.obj["cache"] = Cache(load=ctx.invoked_subcommand != "cache")


main.add_command(cache_command_group, "cache")
main.add_command(config_command_group, "config")
main.add_command(goto_command, "goto")
main.add_command(generate_repos_command, "generate-repos")
main.add_command(open_project_command, "open_project")
main.add_command(activity_command, "activity")

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass

    click.echo("Exiting gracefully ...")
    sys.exit(0)
