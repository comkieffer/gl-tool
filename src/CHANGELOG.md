# Gl-Tool Changelog

## 0.3.1

- Add the `activity` command
- Use `_access_level` attributes instead of `_enabled` attributes on Gitlab Project objects for finaer grained control of which features are enabled.
- Write log files into the user state directory (`~/.local/state`) instead of the current directory
