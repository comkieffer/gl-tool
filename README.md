# Gl-Tool

Gl-Tool (or gitlab-tool) is a very un-imaginatively named project to help you jump straight to the page you want on Gitlab.

When working between different projects in different groups, just navigating to the right repository can be a pain. `gl-tool` user [rofi](https://github.com/davatorium/rofi) to present an interactive picker which automatically filters the list of possible groups and repositories based on what you have typed so far.

![Picture of gl-tool in action](doc/gl-tool-goto.png)

Other features have been implemented around this core:

- Generate a `.repos` file for [vcstool](https://github.com/dirk-thomas/vcstool), a tool to make working with multiple repositories easier (_e.g._ pull multiple repositories at once).
- View a summary of you activity in Gitlab

## Getting Started

Since the project is not on `pip` yet (or ever), the most painless way to install it is

```commandline
pipx install --index-url https://gitlab.com/api/v4/projects/40089099/packages/pypi/simple gl-tool
```

Alternatively, if you do not use `pipx`

```commandline
pip3 install git+https://gitlab.com/comkieffer/gl-tool.git@v0.1.4
```

Run the script by typing `gl-tool` in the terminal. If the command is not found, ensure that `~/.local/bin` is on
your `PATH`.

To get started, we need to provide three pieces of information:

- the instance url (the path to your gitlab instance, e.g. https://gitlab.com),
- a Gitlab _Personal Access Token_ with `read user` and `read api` permissions for that instance,
- a _base group_.

Let's configure `gl-tool` with all this information now

```commandline
gl-tool config set url PATH_TO_INSTANCE_URL
gl-tool config set token YOUR_ACCESS_TOKEN
```

Now refresh the cache with `gl-tool cache refresh` and you're set. Open the picker with `gl-tool goto` and pick your destination. You should see all the projects and groups you own.

## Usage

The full help for the tool can be accessed with `gl-tool --help`.

## Advanced Usage

Long group names can make lines in the picker annoyingly long. To increase the visual density of the output, `gl-tool` provides a _text replacement_ feature. This applies a regex to each project or group name allowing you to replace parts of it with arbitrary text. To get started, add a replacement with

```commandline
gl-tool config add-replacement PATTERN_REGEX REPLACEMENT
```

For example, to transform "My Long Organisation Name" to "Org" in the output, use

```commandline
gl-tool config add-replacement "^My Long Organisation Name" "Org"
```
