FROM ubuntu:22.04

# Setup environment
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -yqq update                             \
    && apt-get -yqq install --no-install-recommends \
        git                                         \
        python3-pip                                 \
    && apt-get -yqq autoremove                      \
    && apt-get -yqq clean                           \
    && rm -rf /var/lib/apt/lists/*                  \
    && pip3 install --no-cache-dir pre-commit poetry
